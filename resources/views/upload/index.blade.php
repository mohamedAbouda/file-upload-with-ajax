@extends('layouts.app')
@section('content')
<form action="{{route('file.store')}}" method="post" enctype="multipart/form-data" id="file-form">
	<input type="hidden" name="_token" value="{{ csrf_token() }}">
	<input type="file" name="file"><br>
	<input type="submit" class="btn btn-success" value="Upload">
</form>
<br>

<table class="table table-bordered"> 
	<thead> <tr> <th>#</th> <th>File</th> <th>Action</th> </tr> 
	</thead> 
	<tbody id="file-table"> 
		@foreach($files as $file)
		<tr> 
			<td>{{$file->id}}</td> 
			<td>{{$file->file_path}}</td> 
			<td><a href="{{ asset( 'uploadedFiles/' . $file->file_path)  }}" download="{{$file->file_path}}">Download</a></td>  </tr>
			@endforeach
		</tbody>
	</table>

	@endsection
	@section('footer')

	@endsection