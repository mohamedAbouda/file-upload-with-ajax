<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Admin Panel</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="{{asset('/css/style.css')}}">
  <link rel="stylesheet" href="{{asset('panel/bootstrap/css/admin_product.css')}}">
  <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
  <link rel="stylesheet" href="{{asset('panel/bootstrap/css/admin_product.css')}}">
  <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">

  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{asset('panel/dist/css/AdminLTE.min.css')}}">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="{{asset('panel/dist/css/skins/_all-skins.min.css')}}">
  <!-- iCheck -->
  <link rel="stylesheet" href="{{asset('panel/plugins/iCheck/flat/blue.css')}}">
  <!-- Morris chart -->
  <link rel="stylesheet" href="{{asset('panel/plugins/morris/morris.css')}}">
  <!-- jvectormap -->
  <link rel="stylesheet" href="{{asset('panel/plugins/jvectormap/jquery-jvectormap-1.2.2.css')}}">
  <!-- Date Picker -->
  <link rel="stylesheet" href="{{asset('panel/plugins/datepicker/datepicker3.css')}}">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="{{asset('panel/plugins/daterangepicker/daterangepicker-bs3.css')}}">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="{{asset('panel/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css')}}">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="{{url('admin')}}" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>A</b>NT</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Ankuat </b>Panel</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

     
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar" style="height: 945px;">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
     
      <!-- search form -->
     
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li class="header">MAIN NAVIGATION</li>
        <li class="treeview">
          <a href="#">
            <i class="glyphicon glyphicon-user"></i> <span>Users</span> <i class="fa fa-angle-left pull-right"></i>
          </a>
         
          <ul class="treeview-menu">

            <li class=""><a href="{{route('admin.user')}}"><i class="fa fa-circle-o"></i>All Users</a></li>
            
           

          </ul>
            </li>
   
              
      
        <li class="treeview">
          <a href="#">
            <i class="glyphicon glyphicon-list"></i> <span>Categories</span> <i class="fa fa-angle-left pull-right"></i>
          </a>
          
          <ul class="treeview-menu">

            <li class=""><a href="{{route('admin.category')}}"><i class="fa fa-circle-o"></i>All categories</a></li>
            <li><a href="{{route('admin.category.create')}}"><i class="fa fa-circle-o"></i>Add Category</a></li>
           
            

          </ul>
           </li>

             <li class="treeview">
          <a href="#">
            <i class="glyphicon glyphicon-align-justify"></i> <span>Sub Categories</span> <i class="fa fa-angle-left pull-right"></i>
          </a>
          
          <ul class="treeview-menu">

            <li class=""><a href="{{route('admin.subcategory')}}"><i class="fa fa-circle-o"></i>All Sub-Categories</a></li>
            <li><a href="{{route('admin.subcategory.create')}}"><i class="fa fa-circle-o"></i>Add Sub-Category</a></li>
           
            

          </ul>
           </li>

              <li class="treeview">
          <a href="#">
            <i class="glyphicon glyphicon-qrcode"></i> <span>Brands</span> <i class="fa fa-angle-left pull-right"></i>
          </a>
          
          <ul class="treeview-menu">

            <li class=""><a href="{{route('admin.brand')}}"><i class="fa fa-circle-o"></i>All Brands</a></li>
            <li><a href="{{route('admin.brand.create')}}"><i class="fa fa-circle-o"></i>Add Brand</a></li>
           
            

          </ul>
           </li>

                <li class="treeview">
          <a href="#">
            <i class="glyphicon glyphicon-resize-full"></i> <span>Sizes</span> <i class="fa fa-angle-left pull-right"></i>
          </a>
          
          <ul class="treeview-menu">

            <li class=""><a href="{{route('admin.size')}}"><i class="fa fa-circle-o"></i>All Sizes</a></li>
            <li><a href="{{route('admin.size.create')}}"><i class="fa fa-circle-o"></i>Add Size</a></li>
           
            

          </ul>
           </li>

        
     
       
                  <li class="treeview">
          <a href="#">
            <i class="glyphicon glyphicon-object-align-top"></i> <span>Products</span> <i class="fa fa-angle-left pull-right"></i>
          </a>
          
          <ul class="treeview-menu">

            <li class=""><a href="{{route('admin.product')}}"><i class="fa fa-circle-o"></i>All Products</a></li>
            <li><a href="{{route('admin.product.create')}}"><i class="fa fa-circle-o"></i>Add Product</a></li>
           
            

          </ul>
           </li>
     
        
       
             <li class="treeview">
          <a href="#">
            <i class="glyphicon glyphicon-dashboard"></i> <span>Deals</span> <i class="fa fa-angle-left pull-right"></i>
          </a>
          
          <ul class="treeview-menu">

            <li class=""><a href="{{route('admin.deal')}}"><i class="fa fa-circle-o"></i>All Deals</a></li>
           
           
            

          </ul>
           </li>


             <li class="treeview">
          <a href="#">
            <i class="glyphicon glyphicon-fullscreen"></i> <span>Main Slider</span> <i class="fa fa-angle-left pull-right"></i>
          </a>
          
          <ul class="treeview-menu">

            <li class=""><a href="{{route('admin.slider')}}"><i class="fa fa-circle-o"></i>All Images</a></li>
            
           
            

          </ul>
           </li>



             <li class="treeview">
          <a href="#">
            <i class="glyphicon glyphicon-briefcase"></i> <span>Orders</span> <i class="fa fa-angle-left pull-right"></i>
          </a>
          
          <ul class="treeview-menu">

            <li class=""><a href="{{route('admin.order')}}"><i class="fa fa-circle-o"></i>All Orders</a></li>
            
           
            

          </ul>
           </li>
      
        
      
    
       
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
   

    <!-- Main content -->
    <section class="content">
     @yield('content')
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
 @include('partials.footerAdmin') 
@yield('footer')

</body>
</html>
