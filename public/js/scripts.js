$('#file-form').submit(function (e){
  e.preventDefault();

  var form = jQuery('#file-form');
  var dataString = new FormData(this);
  var formAction = form.attr('action');
  $.ajax({
    type: "POST",
    url : formAction,
    data : dataString,
    cache:false,
    contentType: false,
    processData: false,
    success : function(data){

      $('#file-table').append(`<tr> 
        <td>`+data.id+`</td> 
        <td>`+data.file_path+`</td> 
        <td><a href="{{ asset( 'uploadedFiles/' . `+data.file_path+`)  }}" download='`+data.file_path+`'>Download</a></td>  </tr>`);


      $('input[type=file]').val('');

      console.log(data);


    },
    error : function(data){
      console.log('error');
    }
  },"json");

});